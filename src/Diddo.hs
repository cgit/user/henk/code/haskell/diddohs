{-# LANGUAGE OverloadedStrings #-}

module Diddo
( LogEntry(LogEntry)
, Diddo(Diddo)
, formatDiddo
, parseDiddoLogline
, logToDiddo
, parseToZonedTime
) where

import Data.Maybe( fromMaybe )
import Data.Time.Clock( UTCTime(), NominalDiffTime(), diffUTCTime )
import Data.Time.Format( FormatTime(), parseTime, formatTime )
import Data.Time.LocalTime( TimeZone(), ZonedTime(..), zonedTimeToUTC, utcToZonedTime )
import Text.Parsec
import System.Locale
import Text.Printf( printf )
import qualified Data.Text as T

data LogEntry = LogEntry
    { timestamp     :: UTCTime
    , timezone      :: TimeZone
    , text          :: T.Text
    }

data Diddo = Diddo
    { startTime     :: ZonedTime
    , endTime       :: ZonedTime
    , comment       :: T.Text
    }

getTimestamp :: Diddo -> UTCTime
getTimestamp = zonedTimeToUTC . endTime

formatDiddo :: String -> Diddo -> T.Text
formatDiddo format (Diddo start end text) = T.intercalate ";" diddoline
    where
        diddoline           = [startZonedString, endZonedString, delta, text]
        startZonedString    = timeToText format start
        endZonedString      = timeToText format end
        startUTC            = zonedTimeToUTC start
        endUTC              = zonedTimeToUTC end
        delta               = diffTimeToHMSString $ diffUTCTime endUTC startUTC

timeToText :: FormatTime a => String -> a -> T.Text
timeToText format = T.pack . formatTime defaultTimeLocale format

logToDiddo :: UTCTime -> LogEntry -> Diddo
logToDiddo startutc logentry = Diddo startZoned endZoned $ text logentry
    where
        startZoned = utcToZonedTime (timezone logentry) startutc
        endZoned = utcToZonedTime (timezone logentry) $ timestamp logentry

-- parseDiddoLogline' :: T.Text -> (UTCTime, LogEntry)
-- parseDiddoLogline' line = (ts, LogEntry ts tz string)

parseDiddoLogline :: T.Text -> (UTCTime, LogEntry)
parseDiddoLogline line = (ts, LogEntry ts tz string)
    where
        (timestring:strings)    = T.splitOn ";" line
        string                  = T.intercalate ";" strings
        time                    = parseISOsecondsTime timestring
        (ts,tz)                 = (zonedTimeToUTC time, zonedTimeZone time)

parseToZonedTime :: String -> String -> ZonedTime
parseToZonedTime format string = zt
    where
        zt = fromMaybe (error $ "Input data broken: " ++ string) parsedTime
        parsedTime = parseTime defaultTimeLocale format string

parseISOsecondsTime :: T.Text -> ZonedTime
parseISOsecondsTime timestring = parseToZonedTime (iso8601DateFormat $ Just "%T%z") $ T.unpack timestring

diffTimeToHMSString :: NominalDiffTime -> T.Text
diffTimeToHMSString delta = T.pack $ printf "%d:%02d:%02d" h m s
    where
        (mLeft, s) = floor delta `divMod` 60 :: (Int, Int)
        (h, m)     = mLeft `divMod` 60

